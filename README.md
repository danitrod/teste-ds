[![](https://img.shields.io/badge/IBM%20Cloud-powered-blue.svg)](https://cloud.ibm.com) [![](https://img.shields.io/badge/firefox-%3E%3D65-brightgreen)](https://www.mozilla.org/pt-BR/firefox/new/) [![](https://img.shields.io/badge/chrome-%3E%3D20-brightgreen)](https://www.google.com/intl/pt-BR/chrome/)

# Desafio Data Science Banco do Brasil

* [1. Introdução](#1-introdução)
* [2. Avaliação](#3-avaliação)
* [3. Requisitos](#4-requisitos)
* [4. Desafio](#2-desafio)

## 1. Introdução (EM CONSTRUÇÃO)
Video Banco do Brasil

[![Watch the video](https://i.imgur.com/vKb2F1B.png)](https://www.youtube.com/embed/t-w-XSbVDsI)

O Banco do Brasil, junto com a IBM, está lançando o Desafio Banco de Brasil de Data Science para fomentar e disseminar o conhecimento e as técnicas da área de dados.

## 2. Avaliação

Por se tratar de um desafio de data science o que será avaliado é a qualidade do modelo criado pelo competidor. Ao final dos notebook fornecidos, existe uma célula cuja função é submeter sua solução para os sistemas de avaliação criados. Uma vez com seu modelo funcionando no Watson Machine Learning Service e com a submissão realizada, o avaliador do respectivo desafio irá fazer inferências no modelo submetido a partir de um dataset de avaliação. A nota do desafio é uma função que compara o valor inferido pelo modelo submetido com o valor real do dataset, avaliando o quão distante o modelo está da realidade e o <img src="https://render.githubusercontent.com/render/math?math=R^2"> do mesmo, que avalia o quão bem o modelo descreve o conjunto de dados.

## 3. Requisitos

Para iniciar o desafio você deverá primeiramente cumprir os seguintes itens:

- Registrar-se na [IBM Cloud](https://ibm.biz/desafio-data-science-bb).

## 4. Desafio

(Sugestão: subtituir por vídeo discutindo sobre DS/ML, produção IBM)

Neste repositório há quatro desafios, dois básicos e dois intermediários, juntos eles cobrem o que é mais utilizado e algumas situações onde é possível aplicar ciência de dados no dia dia.

Para completar o desafio, você irá importar o notebook e o dataset disponibilizado neste repositório, portanto este readme contêm a sequência de passos necessários para que esta tarefa se complete. Faça o download do dataset pois ele será utilizado mais tarde, por enquanto vamos nos concentrar em importar o notebook para o Watson Studio, que será o ambiente onde o desafio deverá ser executado.

1. Acesse sua conta na [IBM Cloud](https://ibm.biz/desafio-data-science-bb). Caso ainda não tenha crie uma conta [aqui](https://ibm.biz/desafio-data-science-bb).

2. Ao logar você deverá ver o seu dashboard. Vamos clicar em catálogo.
![img-dash](./support/img/img-dash.png)

3. No catálogo, na secção de A.I, busque pelo serviço 'Watson Machine Learning Service' e clique nele.
![img-wmlcatalog-view-3](./support/img/img-wmlcatalog-view-3.png)

4. De um nome para o serviço e em seguida clique em 'Create'
![img-wmlcreation](./support/img/img-wmlcreation.png)

5. Após instanciar o serviço você será direcionado para a página incial do mesmo. Vamos criar credenciais de acesso para uso posterior. No lado esquerdo, clique em 'Service Credentials'
![img-wmlinit](./support/img/img-wmlinit.png)

6. Cique em 'New Credential' para criar uma nova credencial do serviço.
![img-wmlcred](./support/img/img-wmlcred.png)

7. De um nome para a credencial e clique em 'Add'.
![img-wmlcredcreation](./support/img/img-wmlcredcreation.png)

8. Uma vez com a credencial criada clieque em 'View credential' para visualizá-las.
![img-wmlviewcred](./support/img/img-wmlviewcred.png)

9. Clique no ícone de copiar conforme mostra a imagem abaixo para copiar suas credencias e salve num bloco de notas, pois ela será utilziada mais tarde. Em seguida clique em 'Resource list' para retornamos para a lista de recursos.
![img-getcred](./support/img/img-getcred.png)

10. Assim que carregar a tela clique em 'Catalog', pois precisamos instanciar o 'Watson Studio'.
![img-wscatalog](./support/img/img-wscatalog.png)

11. Assim que encontrar o 'Watson Studio' clique nele.
![img-wscatalog2](./support/img/img-wscatalog2.png)

12. De um nome para sua instancia e quando estiver pronto clique em 'Create'.
![img-wscreation](./support/img/img-wscreation.png)

13. Assim que seu 'Watson Studio' carregar a página clique em 'Get Started'.
![img-ws](./support/img/img-ws.png)

14. Esta é a tela inicial do Watson Studio, para que possamos utilizar suas ferramentas precisamos criar um projeto, para isso clique em 'Create a project'.
![img-proj](./support/img/img-proj.png)

15. Vamos criar um projeto vazio clicando em 'Create an empty project'.
![img-empt](./support/img/img-empt.png)

16. Caso você não tenha um [Object Storage](https://cloud.ibm.com/catalog/services/cloud-object-storage) será preciso criar um, para isso clique em 'Add'.
![img-prj-creat-ob](./support/img/img-prj-creat-ob.png)

17. Você será direcionado para a tela de criação do 'Object Storage', lá clique em 'Create'.
![img-obcreation](./support/img/img-obcreation.png)

18. De um nome para o seu 'Object Storage' e clique em 'Confirm'.
![img-obconfirm](./support/img/img-obconfirm.png)

19. De volta a tela do 'Watson Studio', clique em 'Refresh'.
![img-refresh](./support/img/img-refresh.png)

20. Seu 'Object Storage' deve aprecer. Agora de um nome para o projeto e clique em 'Create'.
![img-prj-creat](./support/img/img-prj-creat.png)

21. A tela que carregou é o overview do nosso projeto dentro do Watson Studio. Agora vamos importar o notebook disponibilizado para a resolução de um dos desafios, para isso clique em 'Add to project', e em seguida clique em 'notebook'.
![img-add](./support/img/img-add.png)

22. Como vamos importar um notebook, clique em 'From URL', de um nome para o seu notebook, selecione a runtime 'Default Python 3.6 Free (1vCPU 4GB RAM), em seguinda cole umas das seguintes url no campo 'Notebook URL', e selecione a runtime free.
    1. Para o desafio 1: `https://gitlab.com/JoaoPedroPP/teste-ds/-/raw/master/Desafio-1/basico-01.ipynb`
    2. Para o desafio 2: `https://gitlab.com/JoaoPedroPP/teste-ds/-/raw/master/Desafio-2/basico-02.ipynb`
    <!-- 3. Para o desafio 3: ''
    4. Para o desafio 4: '' -->

![img-nb-url](./support/img/img-nb-url.png)

23. Assim que a página carregar, você deverá ver o notebook pronto para uso. Nele você vai encontrar o tema do desafio e a submissão do desafio para avaliação. O restante das instruções são relativas a temática do desafio e elas se encontram dentro de cada notebook fornecido, ou seja, a partir deste ponto o resto das instruções se encontra no notebook de resolução dos desafios.

13. Assim que a página carregar, você deverá ver o notebook pronto para uso. Nele você vai encontrar o tema do desafio e a submissão do desafio para avaliação. O restante das instruções são relativas a temática do desafio e elas se encontram dentro de cada notebook fornecido, ou seja, a partir deste ponto o restante das instruções se encontra no notebook de resolução dos desafios.

## License

Copyright 2019 IBM

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
